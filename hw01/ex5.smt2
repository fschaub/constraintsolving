(declare-const x1 Bool)
(declare-const x2 Bool)
(declare-const x3 Bool)
(declare-const y1 Bool)
(declare-const y2 Bool)
(declare-const y3 Bool)

(declare-fun c1 () Bool)
(declare-fun c2 () Bool) 
(declare-fun z1 () Bool)
(declare-fun z2 () Bool)
(declare-fun z3 () Bool)

(assert (= c1 (and x3 y3)))
(assert (= c2 (or(and x2 y2) (or (and x2 c1) (and y2 c1)))))

(assert (= z3 (xor x3 y3)))
(assert (= z2 (xor x2 (xor y2 c1))))
(assert (= z1 (xor x1 (xor y1 c2))))

(assert (or y1 (or y2 y3)))
(check-sat)
(get-model)
