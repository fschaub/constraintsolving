; people
(declare-fun Agatha () Int)
(declare-fun Butler () Int)
(declare-fun Charles () Int)
; roles
(declare-fun Killer () Int)
(declare-fun Victim () Int)

; SO Vars
(declare-fun hates ((Int)(Int)) Bool)
(declare-fun richerthan ((Int)(Int)) Bool)

; killer hates victim and is not richer
(assert (hates Killer Victim))
(assert (not (richerthan Killer Victim)))

; Charles hates no one that Aunt Agatha hates
(assert (implies (hates Agatha Agatha) (not (hates Charles Agatha))))
(assert (implies (hates Agatha Charles) (not (hates Charles Charles))))
(assert (implies (hates Agatha Butler) (not (hates Charles Butler))))

; Agatha hates everyone except the butler
(assert (implies (not (= Agatha Butler)) (hates Agatha Agatha)))
(assert (implies (not (= Butler Butler)) (hates Agatha Butler)))
(assert (implies (not (= Charles Butler)) (hates Agatha Charles)))

; The butler hates everyone not richer than Aunt Agatha
(assert (implies (not (richerthan Agatha Charles)) (hates Butler Charles)))
(assert (implies (not (richerthan Agatha Agatha)) (hates Butler Agatha)))
(assert (implies (not (richerthan Agatha Butler)) (hates Butler Butler)))

; The butler hates everyone Aunt Agatha hates
(assert (implies (hates Agatha Agatha) (hates Butler Agatha)))
(assert (implies (hates Agatha Butler) (hates Butler Butler)))
(assert (implies (hates Agatha Charles) (hates Butler Charles)))

; No one hates everyone
(assert (not (and (and (hates Agatha Agatha) (hates Agatha Butler)) (hates Agatha Charles))))
(assert (not (and (and (hates Charles Agatha) (hates Charles Butler)) (hates Charles Charles))))
(assert (not (and (and (hates Butler Agatha) (hates Butler Butler)) (hates Butler Charles))))

; Agatha is not the butler
(assert (not (= Agatha Butler)))

; agatha is the victim
(assert (= Victim Agatha))

; someone is the Killer
(assert (or (= Killer Agatha) (or (= Killer Butler) (= Killer Charles))))

(check-sat)
(get-model)
