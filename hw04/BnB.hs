module BnB
    where


import Control.Monad
import Control.Monad.State.Lazy



data Lit = NLit String | Lit String | LConst Bool deriving (Eq)
instance Show Lit where
    show (NLit x) = "-" ++ x
    show (Lit x) = x
    show (LConst b) = if b then "T" else "F"
newtype Clause = Clause [Lit]
instance Show Clause where
    show (Clause []) = "[]"
    show (Clause xs) = foldl1 (\acc x -> x ++ " or " ++ acc) $ map show xs
newtype CNF = CNF [Clause]
instance Show CNF where
    show (CNF []) = "[]"
    show (CNF xs) = foldl1 (\acc x -> x ++ " , " ++ acc) $ map show xs

parseLit :: String -> Lit
parseLit ('-':x) = NLit x
parseLit x = Lit x
parseClause :: String -> Clause
parseClause = Clause . map parseLit . words
parseCNF :: String -> CNF
parseCNF = CNF . map parseClause . lines


numClauses :: CNF -> Int
numClauses (CNF xs) = length xs
vars :: CNF -> [String]
vars (CNF xs) =
    foldl (\acc x -> if x `elem` acc then acc else x:acc) []
        $ concatMap vars' xs
    where
        vars' (Clause ys) = map varfromLit ys
        varfromLit (Lit x) = x
        varfromLit (NLit x) = x
        varfromLit _ = error "cnf contains bools"

simplify :: CNF -> CNF
simplify (CNF xs) = CNF . remTrues $ map filterFalse xs where
    filterFalse (Clause ys) = Clause $ filter isNotFalse ys
    isNotFalse (LConst b) = b
    isNotFalse x = True
    remTrues = filter (not . hasTrues)
    hasTrues (Clause ys) = LConst True `elem` ys

setLit :: Lit -> CNF -> CNF
setLit (LConst _) _ = error "invalid use of setLit"
setLit x (CNF cs) = CNF $ map setLit' cs where
    setLit' (Clause ls) = Clause $ map setLit'' ls
    setLit'' l = case cmpLit l x of
        Neg -> LConst False
        Same -> LConst True
        Uncomp -> l

data CmpLit = Same | Neg | Uncomp
cmpLit :: Lit -> Lit -> CmpLit
cmpLit (LConst _) _ = error "invalid comparison"
cmpLit _ (LConst _) = error "invalid comparison"
cmpLit (Lit x) ly = case ly of
    NLit y -> if y == x then Neg else Uncomp
    Lit y -> if y == x then Same else Uncomp
    -- _ -> undefined
cmpLit (NLit x) ly = case ly of
    NLit y -> if y == x then Same else Uncomp
    Lit y -> if y == x then Neg else Uncomp
    -- _ -> undefined


numEmptys :: CNF -> Int
numEmptys (CNF cs) = length . filter isEmpty $ cs where
    isEmpty (Clause ls) = null ls





theCNF :: CNF
theCNF = parseCNF . unlines $
    [ "-x"
    , "-y"
    , "y x"
    , "-x z"
    , "-y z"
    , "x -y"
    ]




-- | auxillary helper functions
data BnBState = BnBState
    { bnbtrace :: [String]
    , indentation :: Int
    , setlits :: [Lit]
    }

defaultBnBState :: BnBState
defaultBnBState = BnBState [] 0 []

trace' :: String -> BnBState -> BnBState
trace' s bnbs = bnbs {bnbtrace = s : bnbtrace bnbs}

fromBnBState :: BnBState -> String
fromBnBState = unlines . reverse . bnbtrace

type BnB a = State BnBState a
trace :: String -> BnB ()
trace s = do
    ind <- gets indentation
    modify . trace' $ replicate (ind*4) ' ' ++ s
indent :: BnB a -> BnB a
indent f = do
    modify $ \s -> s {indentation = 1 + indentation s}
    r <- f
    modify $ \s -> s {indentation = indentation s - 1}
    return r

withLit :: Lit -> CNF -> (CNF -> BnB a) -> BnB a
withLit l cnf f = do
    modify $ \s -> s {setlits = l : setlits s}
    r <- f $ setLit l cnf
    modify $ \s -> s {setlits = tail $ setlits s}
    return r
getSetLits :: BnB [Lit]
getSetLits = reverse <$> gets setlits

-- | BnB
runBnB :: CNF -> (Int, String)
runBnB cnf = (\(i,t) -> (i, fromBnBState t)) . runState (runBnBM cnf) $ defaultBnBState

runBnBM :: CNF -> BnB Int
runBnBM cnf' = bnb cnf' (numClauses cnf') where
    bnb cnf ub = do
        let cnf1 = simplify cnf
        lits <- show <$> getSetLits
        -- trace $ "\n"
        trace $ "set literals: " ++ lits
        indent $ do
            trace $ "=>    " ++ show cnf
            trace $ "simp: " ++ show cnf1
        if numClauses cnf1 == numEmptys cnf1 then
            return $ numClauses cnf1
        else
            if numEmptys cnf1 >= ub then
                return ub
            else do
                let x = head $ vars cnf1
                indent $ do
                    ub1 <- withLit (Lit x) cnf1 $ \c -> bnb c ub
                    ub2 <- withLit (NLit x) cnf1 $ \c -> bnb c (min ub1 ub)
                    return $ minimum [ub1, ub2, ub]




main :: IO ()
main = do
    let (ub, t) = runBnB theCNF
    putStrLn $ "minUnsat: " ++ show ub
    putStrLn $ "\ntrace: \n" ++ t


--
