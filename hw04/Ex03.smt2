; people
(declare-fun Agatha () Int)
(declare-fun Butler () Int)
(declare-fun Charles () Int)
; roles
(declare-fun Killer () Int)
(declare-fun Victim () Int)

; SO Vars
(declare-fun hates ((Int)(Int)) Bool)
(declare-fun richerthan ((Int)(Int)) Bool)

; killer hates victim and is not richer
(assert (hates Killer Victim))
(assert (not (richerthan Killer Victim)))

; Charles hates no one that Aunt Agatha hates
(assert (forall ((x Int)) (implies (hates Agatha x) (not (hates Charles x)))))

; Agatha hates everyone except the butler
(assert (forall ((x Int)) (xor (and (= x Butler) (not (hates Agatha x))) (hates Agatha x))))

; The butler hates everyone not richer than Aunt Agatha
(assert (forall ((x Int)) (implies (not (richerthan Agatha x)) (hates Butler x))))

; The butler hates everyone Aunt Agatha hates
(assert (forall ((x Int)) (implies (hates Agatha x) (hates Butler x))))

; No one hates everyone
(assert (not (exists ((x Int)) (forall ((y Int)) (hates x y)))))

; Agatha is not the butler
(assert (not (= Agatha Butler)))

; agatha is the victim
(assert (= Victim Agatha))

; someone is the Killer
(assert (or (= Killer Agatha) (or (= Killer Butler) (= Killer Charles))))

(check-sat)
(get-model)
