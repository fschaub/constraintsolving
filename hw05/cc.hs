{-# LANGUAGE LambdaCase #-}
{-# LANGUAGE TypeOperators #-}
import Control.Arrow ((>>>))
import Data.List (sort)
import System.Environment (getArgs)


-- | constant and unary uninterpreted funcitons
data UF = F String UF | X String
  deriving(Eq, Ord)

instance Show UF where
  show (F x y) = x ++ "(" ++ show y ++ ")"
  show (X x) = x


-- | parse UF
newUF :: String -> UF
newUF str
  | null str = error ""
  | length str == 1 = X str
  | head str `notElem` "()" && str !! 1 == '(' = F (take 1 str) $ newUF $ drop 2 str
  | str !! 1 == ')' = X $ take 1 str
  | otherwise = error "no parse UF ¯\\_(^^)_/¯"


-- | head symbol of UF
head' :: UF -> String
head' (F x _) = x
head' (X x)   = x

-- | Maybe inner UF, Nothing if Constant Fun
tail' :: UF -> Maybe UF
tail' (F _ x) = Just x
tail' _       = Nothing

-- | equality over uninterpreted functions
data EUF = UF :=: UF | UF :/=: UF
  deriving(Show,Eq,Ord)

-- | parse EUF from String
euf :: String -> EUF
euf str = eqineq eqSym (newUF fstUF) (newUF sndUF) where
  eqineq x
    | x == "=" = (:=:)
    | x == "/=" = (:/=:)
    | otherwise = error "invalid equality"
  eqSym = dropWhile (`notElem` "/=") >>> takeWhile (`elem` "/=") $ str
  fstUF = filter (/= ' ') . takeWhile (`notElem` "/=") $ str
  sndUF = filter (/= ' ') . dropWhile (`elem` "/=") . dropWhile (`notElem` "/=") $ str


-- | check if EUF is an equality
isEq :: EUF -> Bool
isEq (_ :=: _) = True
isEq _        = False


-- | congruence closure
cc :: [EUF] -> [[[UF]]]
cc = filter isEq >>> construct >>> asSet >>> \xs -> xs : cc' xs where
  -- | construct all initial equality classes and subterms
  construct = concatMap $ \case
    (x :=: y) -> asSet [x,y] :
      case (tail' x, tail' y) of
        (Nothing, Nothing) -> []
        (Nothing, Just b) -> construct [b :=: b]
        (Just a, Nothing) -> construct [a :=: a]
        (Just a, Just b) -> construct [a :=: a, b :=: b]
    _         -> error ""
  -- | merge all sets with same elements
  mergeSimple :: [[UF]] -> [[UF]]
  mergeSimple xss = asSet $ foldl insert mempty $ asSet $ map asSet xss
  insert [] xs = pure xs
  insert (ys:yss) xs
    | any (`elem` xs) ys = asSet (xs ++ ys) : yss
    | otherwise = asSet ys : insert yss xs
  -- | perform congruence closure until fixpoint is reached
  cc' :: [[UF]] -> [[[UF]]]
  cc' xss =
    let xss' = mergeSimple xss
        yss = foldl mergeCongruent xss' xss'
    in xss' : if xss' == yss then [yss] else cc' yss
  mergeCongruent yss xs = asSet $ yss
    ++ [ asSet (as ++ bs) | x<-xs
                  , y<-xs
                  , x /= y
                  , head' x == head' y
                  , as<-yss
                  , bs<-yss
                  , as /= bs
                  , tail' x `elem` sequence (pure as)
                  , tail' y `elem` sequence (pure bs)
                  ]

-- | make a "set" of a list
asSet :: (Eq a, Ord a) => [a] -> [a]
asSet = sort . foldl (\acc x -> if x `elem` acc then acc else x:acc) []


-- | read first arg as filename, parse file and perform CC
main :: IO ()
main = do
  file <- head <$> getArgs
  eufs <- map euf . lines <$> readFile file
  putStrLn "calculating congruence closure for:"
  putStrLn $ unlines . map show $ eufs
  putStrLn ""
  let cctrace = cc eufs
  mapM_ (\(step, ccstep) -> do
    putStrLn $ "cc step" ++ show step ++ ":"
    putStrLn . unlines $ map show ccstep
    putStrLn "\n"
    ) $ zip [0 :: Int ..] cctrace
