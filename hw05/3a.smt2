(declare-sort Var)
(declare-fun G (Var) Var)
(declare-const x Var)
(assert (not (= x (G x))))
(assert (= (G x) (G (G x))))
(assert (= (G (G (G (G (G (G x)))))) x))
(check-sat)
